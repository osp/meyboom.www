const studios = document.querySelectorAll('.studio')
const room_summaries = document.querySelectorAll('.studio summary')
const room_links = document.querySelectorAll('svg .room');
const top_link = document.getElementById('top-link');

let updating_state = false;

function isInViewport(element) {
    const rect = element.getBoundingClientRect()
    return (
        rect.top >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) * 3/4
    )
}

function update_active(id){

    // reset all room and studio
    for(let rl of room_links){
        rl.classList.remove('active');
    }
    for(let st of studios){
        st.classList.remove('active');
    }
    
    if(id != ''){
        let studio = document.getElementById(id);
    
        // coloring room and studio
        if(id != 'studio-info'){
            let link = document.getElementById('svg__' + id);
            studio.classList.add('active');
            link.classList.add('active');
        }
    }
}

function updateURLState(key) {
    const url = new URL(location);
    url.hash = key;
    history.pushState({}, "", url);
    update_active(key);
}

function update_state(){
    // update the dom state according to the hash
    
    // get hash
    let id = window.location.hash.slice(1);
    
    if(id != ''){
        updating_state = true;
        let studio = document.getElementById(id);

        update_active(id);

        // open studio <detail>
        if(studio.getAttribute('open') != ''){
            studio.setAttribute('open', '');
        }

        // now that <detail> is open go to hash again
        setTimeout(function(){
            window.location.hash = window.location.hash;
            setTimeout(function(){
                updating_state = false;
            },0);
        },0);
    }
}

function update_selected(){
    if (! updating_state){
        let studios_array = Array.prototype.slice.call(studios);
        studios_array.reverse();
        for(let studio of studios_array){
            if(studio.getAttribute('open') == ''
                || studio.id == 'studio-info'){
                if (isInViewport(studio)) {               
                    updateURLState(studio.id);
                }
            }
        }
    }
}

//  added to default
//  -------------------------------------------------------

function click_summary(link){
    // default: open details
    // added: change url so it scroll over there
    let studio = link.parentElement;
    if(studio.getAttribute('open') != ''){
        setTimeout(function(){
            window.location.hash = '#' + studio.id;
        }, 0);
    }
    // we are closing the one active
    else if(window.location.hash == '#' + studio.id){
        updateURLState('');
        setTimeout(function(){
            update_selected();
        }, 0);
    }
}
for(let room_summary of room_summaries){
    room_summary.addEventListener('click', function(){
        click_summary(room_summary);
    });
}
function click_top(){
    // default: goes to top
    // added: closes all details
    console.log('closed');
    for (let studio of studios){
        studio.removeAttribute('open');
    }
}
top_link.addEventListener('click', function(){
    click_top();
});

//  everytime we scroll
//  -------------------------------------------------------

window.addEventListener('scroll', update_selected);

if ("onhashchange" in window) { // event supported?
    window.onhashchange = function () {
        update_state();
    }
}
// when we arrive on an HASH
update_state();