AUTHOR = 'Meyboom'
SITENAME = 'Meyboom'
SITEURL = ''

PATH = 'content'
THEME = 'themes'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


# ARCHITECTURE
# ==============================================================================

# do not generate main site (article and pages)
ARTICLE_SAVE_AS = ARTICLE_LANG_SAVE_AS = ''
ARTICLE_URL = ARTICLE_LANG_URL = ''
# PAGE_SAVE_AS = PAGE_LANG_SAVE_AS = ''
# PAGE_URL = PAGE_LANG_URL = ''

# do not generate direct template
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORY_SAVE_AS = ''

# PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['bios', 'circles']
STATIC_PATHS = ['images']

# generate direct template (that are not equivalent to one file in the content folder)
DIRECT_TEMPLATES = ['index']

USE_FOLDER_AS_CATEGORY = True
PATH_METADATA = "(?P<category>.*)/.*"

DEFAULT_DATE = 'fs'

# for event_date parsing
LOCALE = 'en_US.utf8'

SUMMARY_MAX_LENGTH = 42


# MD EXTENSION
# ==============================================================================

from markdown import Markdown, markdown
import yafg
# https://pypi.org/project/yafg/

# from markdown_figcap import FigCapExtension
# https://github.com/funk1d/markdown-figcap

# from zettlr2pelican import Zettlr2PelicanExtension

MARKDOWN = {
    'extensions': [
    ],
    'extension_configs': {
        'yafg': {
            'stripTitle': 'False',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

# for officially and third party extensions:
# https://python-markdown.github.io/extensions/


# FILTER
# ==============================================================================

import datetime

def md_parse(value):
    # to parse yaml field as markdown
    return markdown(value, extensions=MARKDOWN['extensions'], extension_configs=MARKDOWN['extension_configs'])

def has_cat(articles, cat):
    # return only articles that have a specific category amongst
    # they categories ancestors
    return [article for article in articles if str(cat) in str(article.category)]

def sort_by_order(articles):
    # return only articles that have a specific category amongst
    # they categories ancestors
    return sorted(articles, key=lambda x: int(x.order), reverse=False)

JINJA_FILTERS = {
    'has_cat': has_cat,
    'md_parse': md_parse,
    'sort_by_order': sort_by_order,
}

# PLUGINS
# ==============================================================================

PLUGIN_PATHS = ['plugins']

PLUGINS = ['more_categories', 'links_genres', 'data_files']

# --- MORE CATEGORIES
# let's really use the fact that the CMS (nextcloud) is a nested structure :)
# allow nested categories
# https://github.com/pelican-plugins/more-categories

# --- data_files (from: https://github.com/LucasVanHaaren/pelican-data-files, edited with custom added YAML support)
# by parse the json and yaml file in the data/ directory and add them tp the context
# as object with name "DATA_[filename]"
# DATA_FILES_DIR = "config/"